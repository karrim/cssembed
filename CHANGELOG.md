# Changelog

## 0.2.0+5

- Updated example.

## 0.2.0+4

- Renamed transformer.dart to cssembed.dart.

## 0.2.0+3

- Edited before publishing to pub.

## 0.2.0+2

- Rewritten changelog. Homepage added.

## 0.2.0+1

- Removed unnecessary print.

## 0.2.0

- Now css is embedded into existing style tag.

## 0.1.0

- First working version.

## 0.0.1

- Initial version, created by Stagehand

