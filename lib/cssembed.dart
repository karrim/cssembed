/// Copyright (C) 2015  Andrea Cantafio
///
/// This program is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License along
/// with this program; if not, write to the Free Software Foundation, Inc.,
/// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
library cssembed.lib.transformer;

import 'package:barback/barback.dart';
import 'package:html/parser.dart';
import 'package:html/dom.dart';
import 'package:path/path.dart';

/// cssembed transformer.
class CssEmbed extends AggregateTransformer {
  CssEmbed.asPlugin();

  @override
  apply(AggregateTransform transform) async {
    Asset cssAsset, htmlAsset;
    await for (Asset asset in transform.primaryInputs) {
      if ('.css' == asset.id.extension) {
        cssAsset = asset;
      }
      if ('.html' == asset.id.extension) {
        htmlAsset = asset;
      }
    }
    if (null == cssAsset || null == htmlAsset) {
      return;
    }
    final css = await cssAsset.readAsString(),
        html = await htmlAsset.readAsString();

    final doc = parse(html);
    if(null == doc.querySelector('dom-module')) {
      return;
    }
    final style = new Element.tag('style')..innerHtml = css;
    doc.querySelector('template').append(style);

    transform.consumePrimary(cssAsset.id);
    transform.addOutput(new Asset.fromString(htmlAsset.id, doc.outerHtml));
  }

  @override
  classifyPrimary(AssetId id) {
    final path = id.path;
    switch (extension(path)) {
      case '.css':
      case '.html':
        return withoutExtension(path);
      default:
        return null;
    }
  }
}
