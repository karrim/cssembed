# cssembed

Embeds css into polymer elements.

## Requirements

* Polymer 1.0

## Usage

Add `cssembed` to `pubspec.yaml` before `polymer` transformer:

    transformers:
    - cssembed
    - polymer:
        entry_points: web/index.html
    - $dart2js:
        minify: true
        commandLineOptions:
        - --trust-type-annotations
        - --trust-primitives
        
Stylesheets must have the same name of the polymer element and must be placed in the same directory.

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://bitbucket.org/karrim/cssembed/issues
